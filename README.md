INTRODUCTION
============

- Review drupal code via GPT.

REQUIREMENTS
============

- Drupal `10.x` or higher (The latest version of Drupal `10.x`)

INSTALLATION
============

- `composer require drupal/gpt_code_reviewer`
