<?php

namespace Drupal\gpt_code_reviewer\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\gpt_code_reviewer\Service\ReviewServiceInterface;
use GuzzleHttp\ClientInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form controller for Review create or edit forms.
 *
 * @ingroup gpt_code_reviewer
 */
class ReviewForm extends ContentEntityForm {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The review service.
   *
   * @var \Drupal\gpt_code_reviewer\Service\ReviewServiceInterface
   */
  protected $reviewService;

  /**
   * Guzzle client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * ReviewForm constructor.
   *
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\gpt_code_reviewer\Service\ReviewServiceInterface $review_service
   *   The Review service.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   Guzzle Http Client.
   */
  public function __construct(EntityRepositoryInterface $entity_repository,
                              EntityTypeBundleInfoInterface $entity_type_bundle_info,
                              TimeInterface $time,
                              AccountInterface $current_user,
                              ReviewServiceInterface $review_service,
                              ClientInterface $http_client) {
    parent::__construct($entity_repository, $entity_type_bundle_info, $time);

    $this->currentUser = $current_user;
    $this->reviewService = $review_service;
    $this->httpClient = $http_client;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.repository'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time'),
      $container->get('current_user'),
      $container->get('gpt_code_reviewer.review_service'),
      $container->get('http_client')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildForm($form, $form_state);

    $entity = $this->entity;

    $form['merge_request_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Merge request URL'),
      '#default_value' => $entity->getMergeRequestUrl(),
      '#placeholder' => 'https://git.example.com/project/{PROJECT_NAME}/-/merge_requests/{MERGE_REQUEST_NUMBER}',
      '#description' => $this->t('e.g. https://git.drupalcode.org/project/gpt_code_reviewer/-/merge_requests/1234'),
      '#ajax' => [
        'callback' => '::getGitAjaxCallback',
        'event' => 'change',
        'wrapper' => 'git-container',
      ],
    ];

    $git_info = $this->getGitInfo($form_state->getValue('merge_request_url'));

    $form['git_container'] = [
      '#type' => 'container',
      '#attributes' => [
        'id' => 'git-container',
      ],
    ];

    $form['git_container']['repo_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Git repository URL'),
      '#required' => TRUE,
      '#default_value' => $entity->getRepoUrl(),
    ];
    if (!empty($git_info['repo_url'])) {
      $form['git_container']['repo_url']['#value'] = $git_info['repo_url'];
    }

    $form['git_container']['branch'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Branch'),
      '#required' => TRUE,
      '#default_value' => $entity->getBranch(),
    ];
    if (!empty($git_info['branch'])) {
      $form['git_container']['branch']['#value'] = $git_info['branch'];
    }

    $form['git_container']['base_commit_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Base commit ID'),
      '#required' => TRUE,
      '#default_value' => $entity->getBaseCommitId(),
    ];
    if (!empty($git_info['base_commit_id'])) {
      $form['git_container']['base_commit_id']['#value'] = $git_info['base_commit_id'];
    }

    $form['git_container']['commit_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Latest commit ID'),
      '#required' => TRUE,
      '#default_value' => $entity->getCommitId(),
    ];
    if (!empty($git_info['commit_id'])) {
      $form['git_container']['commit_id']['#value'] = $git_info['commit_id'];
    }

    unset($form['uid']);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    $fields = [
      'merge_request_url',
      'repo_url',
      'branch',
      'base_commit_id',
      'commit_id',
    ];

    foreach ($fields as $field) {
      $form_state->setValue($field, trim($form_state->getValue($field)));
    }

    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state): void {
    $entity = $this->entity;

    $params = [
      'repo_url' => $entity->getRepoUrl(),
      'branch' => $entity->getBranch(),
      'base_commit_id' => $entity->getBaseCommitId(),
      'commit_id' => $entity->getCommitId(),
    ];

    try {
      $result = $this->reviewService->review($params);
      $entity->setReviewResult($result['result']);
      $entity->save();

      $form_state->setRedirect('entity.gpt_code_reviewer_review.canonical', [
        'gpt_code_reviewer_review' => $entity->id(),
      ]);
    }
    catch (\Exception $e) {
      $this->messenger()->addError($this->t('An error occurred: @exception', [
        '@exception' => $e->getMessage(),
      ]));

      $form_state->setRedirect('entity.gpt_code_reviewer_review.collection');
    }

  }

  /**
   * Ajax callback when a merge request is input.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state interface element.
   *
   * @return array
   *   Form element for instance_ip_container.
   */
  public function getGitAjaxCallback(array $form, FormStateInterface $form_state): array {
    return $form['git_container'];
  }

  /**
   * Get Git information according to merge request URL.
   *
   * @param string $merge_request_url
   *   The merge request URL.
   *
   * @return array
   *   Git information.
   */
  private function getGitInfo($merge_request_url): array {
    if (empty($merge_request_url)) {
      return [];
    }

    // Parse merge request url.
    $parsed_url = parse_url($merge_request_url);
    $scheme = $parsed_url['scheme'];
    $host = $parsed_url['host'];
    $path = $parsed_url['path'];
    $pattern = '/\/project\/(\w+)\/-\/merge_requests\/(\d+)/';
    if (!preg_match($pattern, $path, $matches)) {
      return [];
    }

    $project_name = $matches[1];
    $merge_request_id = $matches[2];

    // Get project ID.
    $response = $this->httpClient->request('GET', "$scheme://$host/api/v4/projects/project%2F$project_name", [
      'headers' => [
        'Accept' => 'application/json',
      ],
    ]);

    if ($response->getStatusCode() !== 200) {
      return [];
    }

    $data = json_decode($response->getBody(), TRUE);
    $project_id = $data['id'];
    $repo_url = $data['http_url_to_repo'];

    // Get commit IDs.
    $response = $this->httpClient->request('GET', "$scheme://$host/api/v4/projects/$project_id/merge_requests/$merge_request_id", [
      'headers' => [
        'Accept' => 'application/json',
      ],
    ]);

    if ($response->getStatusCode() !== 200) {
      return [];
    }

    $data = json_decode($response->getBody(), TRUE);

    $base_commit_id = $data['diff_refs']['base_sha'];
    $commit_id = $data['diff_refs']['head_sha'];
    $branch = $data['source_branch'];

    // Overwrite $repo_url to the forked repo URL if it exists by checking
    // 'source_project_id'.
    if (!empty($data['source_project_id'])) {
      $response = $this->httpClient->request('GET', "$scheme://$host/api/v4/projects/{$data['source_project_id']}", [
        'headers' => [
          'Accept' => 'application/json',
        ],
      ]);

      if ($response->getStatusCode() !== 200) {
        return [];
      }

      $data = json_decode($response->getBody(), TRUE);
      if (!empty($data['http_url_to_repo'])) {
        $repo_url = $data['http_url_to_repo'];
      }
    }

    if (empty($repo_url)
      || empty($branch)
      || empty($base_commit_id)
      || empty($commit_id)) {
      return [];
    }

    return [
      'repo_url' => $repo_url,
      'branch' => $branch,
      'base_commit_id' => $base_commit_id,
      'commit_id' => $commit_id,
    ];
  }

}
