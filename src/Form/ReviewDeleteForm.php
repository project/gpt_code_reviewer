<?php

namespace Drupal\gpt_code_reviewer\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Builds the form to delete Review entities.
 */
class ReviewDeleteForm extends ContentEntityDeleteForm {

}
