<?php

namespace Drupal\gpt_code_reviewer\Form\Config;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class GPT Code Reviewer Admin Settings.
 */
class GptCodeReviewerAdminSettings extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'gpt_code_reviewer_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['gpt_code_reviewer.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config('gpt_code_reviewer.settings');

    // OpenAI API settings fieldset.
    $form['openai_api_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('OpenAI API'),
      '#open' => TRUE,
    ];

    $form['openai_api_settings']['gpt_api_server'] = [
      '#type' => 'textfield',
      '#title' => 'GPT Code Reviewer API server',
      '#description' => $this->t('The URL of GPT API server.'),
      '#default_value' => $config->get('gpt_api_server'),
      '#required' => TRUE,
    ];

    $form['openai_api_settings']['openai_api_key'] = [
      '#type' => 'textfield',
      '#title' => 'OpenAI API key',
      '#description' => $this->t('The API key of the OpenAI.'),
      '#default_value' => $config->get('openai_api_key'),
      '#required' => TRUE,
    ];

    // Add a dropdown for selecting the OpenAI API model.
    $form['openai_api_settings']['openai_api_model'] = [
      '#type' => 'select',
      '#title' => $this->t('OpenAI API Model'),
      '#description' => $this->t('Select the model used by the OpenAI API.'),
      '#options' => [
        'gpt-3.5-turbo' => $this->t('gpt-3.5-turbo'),
        'gpt-4' => $this->t('gpt-4'),
        'gpt-4-turbo' => $this->t('gpt-4-turbo'),
      ],
      '#default_value' => $config->get('openai_api_model', 'gpt-4-turbo') ?: 'gpt-4-turbo',
      '#required' => TRUE,
    ];

    // Add a timeout field within the OpenAI API settings.
    $form['openai_api_settings']['openai_api_timeout'] = [
      '#type' => 'number',
      '#title' => $this->t('Timeout'),
      '#description' => $this->t('The timeout for the API request in seconds.'),
      '#default_value' => $config->get('openai_api_timeout') ?: 600,
      '#required' => TRUE,
      '#min' => 600,
      '#max' => 1800,
      '#step' => 60,
      '#attributes' => ['style' => 'max-width: 100px;'],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $config = $this->configFactory()->getEditable('gpt_code_reviewer.settings');
    $config->set('gpt_api_server', $form_state->getValue('gpt_api_server'))
      ->set('openai_api_key', $form_state->getValue('openai_api_key'))
      ->set('openai_api_model', $form_state->getValue('openai_api_model'))
      ->set('openai_api_timeout', $form_state->getValue('openai_api_timeout'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
