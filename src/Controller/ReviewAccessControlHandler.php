<?php

namespace Drupal\gpt_code_reviewer\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Access controller for the Review entity.
 *
 * @see \Drupal\gpt_code_reviewer\Entity\Review.
 */
class ReviewAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account): AccessResultInterface {

    switch ($operation) {
      case 'view':
        return AccessResult::allowedIfHasPermission($account, 'view gpt_code_reviewer review');

      case 'update':
      case 'edit':
        return AccessResult::allowedIfHasPermission($account, 'edit gpt_code_reviewer review');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete gpt_code_reviewer review');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add gpt_code_reviewer review');
  }

}
