<?php

namespace Drupal\gpt_code_reviewer\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\link\LinkItemInterface;
use Drupal\user\UserInterface;

/**
 * Defines the review entity.
 *
 * @ingroup gpt_code_reviewer
 *
 * @ContentEntityType(
 *   id = "gpt_code_reviewer_review",
 *   id_plural = "gpt_code_reviewer_reviews",
 *   label = @Translation("Review"),
 *   label_collection = @Translation("Reviews"),
 *   label_singular = @Translation("Review"),
 *   label_plural = @Translation("Reviews"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\gpt_code_reviewer\Controller\ReviewListBuilder",
 *     "views_data"   = "Drupal\gpt_code_reviewer\Entity\ReviewViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\gpt_code_reviewer\Form\ReviewForm",
 *       "add" = "Drupal\gpt_code_reviewer\Form\ReviewForm",
 *       "edit" = "Drupal\gpt_code_reviewer\Form\ReviewForm",
 *       "delete" = "Drupal\gpt_code_reviewer\Form\ReviewDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\gpt_code_reviewer\Controller\ReviewAccessControlHandler",
 *   },
 *   base_table = "gpt_code_reviewer_review",
 *   admin_permission = "administer gpt_code_reviewer review",
 *   fieldable = TRUE,
 *   entity_keys = {
 *     "id"    = "id",
 *     "label" = "name",
 *     "uuid"  = "uuid",
 *   },
 *   links = {
 *     "canonical" = "/gpt_code_reviewer/review/{gpt_code_reviewer_review}",
 *     "add-form" = "/gpt_code_reviewer/review/add",
 *     "edit-form" = "/gpt_code_reviewer/review/{gpt_code_reviewer_review}/edit",
 *     "delete-form" = "/gpt_code_reviewer/review/{gpt_code_reviewer_review}/delete",
 *     "collection" = "/gpt_code_reviewer/review",
 *   },
 *   field_ui_base_route = "gpt_code_reviewer_review.settings"
 * )
 */
class Review extends ContentEntityBase implements ReviewInterface {

  /**
   * {@inheritdoc}
   */
  public function getName(): ?string {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name = ''): ReviewInterface {
    return $this->set('name', $name);
  }

  /**
   * {@inheritdoc}
   */
  public function getMergeRequestUrl(): ?string {
    $item = $this->get('merge_request_url')->first();
    return !empty($item)
      ? $this->get('merge_request_url')->first()->getUrl()->toString()
      : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function setMergeRequestUrl($merge_request_url = ''): ReviewInterface {
    return $this->set('merge_request_url', $merge_request_url);
  }

  /**
   * {@inheritdoc}
   */
  public function getRepoUrl(): ?string {
    $item = $this->get('repo_url')->first();
    return !empty($item)
      ? $this->get('repo_url')->first()->getUrl()->toString()
      : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function setRepoUrl($repo_url = ''): ReviewInterface {
    return $this->set('repo_url', $repo_url);
  }

  /**
   * {@inheritdoc}
   */
  public function getBranch(): ?string {
    return $this->get('branch')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setBranch($branch = ''): ReviewInterface {
    return $this->set('branch', $branch);
  }

  /**
   * {@inheritdoc}
   */
  public function getBaseCommitId(): ?string {
    return $this->get('base_commit_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setBaseCommitId($base_commit_id = ''): ReviewInterface {
    return $this->set('base_commit_id', $base_commit_id);
  }

  /**
   * {@inheritdoc}
   */
  public function getCommitId(): ?string {
    return $this->get('commit_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCommitId($commit_id = ''): ReviewInterface {
    return $this->set('commit_id', $commit_id);
  }

  /**
   * {@inheritdoc}
   */
  public function getReviewResult(): ?string {
    return $this->get('review_result')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setReviewResult($review_result = ''): ReviewInterface {
    return $this->set('review_result', $review_result);
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('uid')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    return $this->set('uid', $account->id());
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('uid')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    return $this->set('uid', $uid);
  }

  /**
   * {@inheritdoc}
   */
  public function created() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function changed() {
    return $this->get('changed')->value;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {

    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the Review entity.'))
      ->setReadOnly(TRUE);

    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The UUID of the Review entity.'))
      ->setReadOnly(TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['merge_request_url'] = BaseFieldDefinition::create('link')
      ->setLabel(t('Merge Request URL'))
      ->setDescription(t('The merge request URL.'))
      ->setSettings([
        'link_type' => LinkItemInterface::LINK_EXTERNAL,
        'title' => DRUPAL_OPTIONAL,
      ])
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'link',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['repo_url'] = BaseFieldDefinition::create('link')
      ->setLabel(t('Git Repository URL'))
      ->setDescription(t('The Git repository URL.'))
      ->setSettings([
        'link_type' => LinkItemInterface::LINK_EXTERNAL,
        'title' => DRUPAL_OPTIONAL,
      ])
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'link',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['branch'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Git Branch'))
      ->setDescription(t('The Git branch.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['base_commit_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Base Commit ID'))
      ->setDescription(t('The base commit ID.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['commit_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Commit ID'))
      ->setDescription(t('The commit ID.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['review_result'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('Review Result'))
      ->setDescription(t('The review result.'))
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'review_result_formatter',
        'weight' => -5,
      ])
      ->setReadOnly(TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('When the entity was created.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'timestamp',
        'weight' => -5,
        'settings' => [
          'date_format' => 'short',
        ],
      ]);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'timestamp',
        'weight' => -5,
        'settings' => [
          'date_format' => 'short',
        ],
      ]);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('Enter the user ID of the Review entity author.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDefaultValueCallback('Drupal\node\Entity\Node::getDefaultEntityOwner')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'match_limit' => 10,
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

}
