<?php

namespace Drupal\gpt_code_reviewer\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides the views data for the review entity type.
 */
class ReviewViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData(): array {
    $data = parent::getViewsData();

    return $data;
  }

}
