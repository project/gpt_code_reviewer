<?php

namespace Drupal\gpt_code_reviewer\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining Review entity.
 *
 * @ingroup gpt_code_reviewer
 */
interface ReviewInterface extends ContentEntityInterface, EntityOwnerInterface {

  /**
   * {@inheritdoc}
   */
  public function getName(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setName($name = ''): ReviewInterface;

  /**
   * {@inheritdoc}
   */
  public function getMergeRequestUrl(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setMergeRequestUrl($merge_request_url = ''): ReviewInterface;

  /**
   * {@inheritdoc}
   */
  public function getRepoUrl(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setRepoUrl($repo_url = ''): ReviewInterface;

  /**
   * {@inheritdoc}
   */
  public function getBranch(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setBranch($branch = ''): ReviewInterface;

  /**
   * {@inheritdoc}
   */
  public function getBaseCommitId(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setBaseCommitId($base_commit_id = ''): ReviewInterface;

  /**
   * {@inheritdoc}
   */
  public function getCommitId(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setCommitId($commit_id = ''): ReviewInterface;

  /**
   * {@inheritdoc}
   */
  public function getReviewResult(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setReviewResult($review_result = ''): ReviewInterface;

  /**
   * {@inheritdoc}
   */
  public function created();

  /**
   * {@inheritdoc}
   */
  public function changed();

}
