<?php

namespace Drupal\gpt_code_reviewer\Service;

use Drupal\Core\Config\ConfigFactoryInterface;
use GuzzleHttp\ClientInterface;

/**
 * ReviewService service interacts with the Review API.
 */
class ReviewService implements ReviewServiceInterface {

  /**
   * The config factory.
   *
   * Subclasses should use the self::config() method (or use
   * ConfigFormBaseTrait), which may be overridden to address specific needs
   * when loading config, rather than this property directly.
   * See \Drupal\Core\Form\ConfigFormBase::config() for an example of this.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Guzzle client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * Constructs a new ReviewService object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   A configuration factory.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   Guzzle Http Client.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    ClientInterface $http_client
  ) {
    $this->configFactory = $config_factory;
    $this->httpClient = $http_client;
  }

  /**
   * {@inheritdoc}
   */
  public function review(array $params = []): ?array {
    $config = $this->configFactory->get('gpt_code_reviewer.settings');
    $gpt_api_server = $config->get('gpt_api_server');
    $params['openai_api_key'] = $config->get('openai_api_key');
    $params['model'] = $config->get('openai_api_model');

    $options = [
      'body' => json_encode($params, JSON_THROW_ON_ERROR),
      'headers' => [
        'Content-Type' => 'application/json',
      ],
      'timeout' => $config->get('openai_api_timeout') ?: self::API_TIMEOUT,
    ];
    $response = $this->httpClient->post(
      $gpt_api_server,
      $options
    );

    return json_decode($response->getBody()->getContents(), TRUE);
  }

}
