<?php

namespace Drupal\gpt_code_reviewer\Service;

/**
 * Provides ReviewService interface.
 */
interface ReviewServiceInterface {

  public const API_TIMEOUT = 600;

  /**
   * Review.
   *
   * @param array $params
   *   Parameters array to send to API.
   *
   * @return mixed
   *   An array of results or NULL if there is an error.
   */
  public function review(array $params = []): ?array;

}
