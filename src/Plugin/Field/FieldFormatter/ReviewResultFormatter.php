<?php

namespace Drupal\gpt_code_reviewer\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the 'review_result_formatter' formatter.
 *
 * This formatter render the content of review result.
 *
 * @FieldFormatter(
 *   id = "review_result_formatter",
 *   label = @Translation("Review result string formatter"),
 *   field_types = {
 *     "string_long",
 *   }
 * )
 */
class ReviewResultFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {
    $elements = [];

    foreach ($items ?: [] as $delta => $item) {
      $elements[$delta] = [
        '#markup' => '<pre><code>' . $item->value . '</code></pre>',
        '#attached' => [
          'library' => ['gpt_code_reviewer/review_result'],
        ],
      ];
    }

    return $elements;
  }

}
